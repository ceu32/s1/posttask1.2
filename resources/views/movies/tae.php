@extends('account.layout')

@section('content')

	<div class="row	align-items-center">
	<div class="col-lg-12 margin-tb">
	<div class="pull-left">

	<h2 class="text-center pt-5">NEW ACCOUNT</h2>
	</div>
	<div class="pull-right">
    </div>
    </div>
</div>

@if ($errors->any())

	<div class="alert alert-danger">
	<strong>UH OH!</strong> There's a problem with your input!<br><br>
	<ul>
@foreach ($errors->all() as $error)
	<li>{{ $error }}</li>
	@endforeach

</ul>
</div>
@endif

<form action="{{ route('account.store') }}" method="POST">
@csrf

	<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12">
	<div class="form-group">
	<strong>Account Name:</strong>
	<input type="text" name="account" class="txt form-control" placeholder="Account">
	
</div>
</div>

	<div class="col-xs-12 col-sm-12 col-md-12">
	<div class="form-group">
	<strong>Level</strong>

	<input type="text" name="lvl" class="txt form-control" placeholder="Level">

</div>
</div>

	<div class="col-xs-12 col-sm-12 col-md-12">
	<div class="form-group">
	<strong>Email</strong>
	<input type="text" name="email" class="txt form-control" placeholder="Email">

</div>
</div>

	<div class="container">
	<div class="row2">
		<div class="col-md-2">
		<a class="btn btn-primary" href="{{ route('account.index') }}">Back</a>
		</div>

	<div class= "row3">	
		<div class="col-md-2">
		<button type="submit" class="btn btn-primary">Submit</button>
		</div><!--Col-md-2-->
</div><!--Row 3-->
	</div><!--Row2-->
	</div><!--Container-->
</div><!--row-->
</form>
@endsection