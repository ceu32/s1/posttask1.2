@extends('movies.layout')

@section('content')

	<dic class="row align-items-center">
	<div class="col-lg-12 margin-tb">
	<div class="pull-left">

	<h2 class="text-center pt-5"> Edit Account</h2>
</div>
	<div class="pull-right">
	</div>
	
</div>
</div>

@if ($errors->any())
	<div class="alert alert-danger">
	<strong>UH OH!</strong>There's a problem with your input! <br><br>

	<ul>

	@foreach ($errors->all() as $error)

	<li>{{ $error }}</li>

	@endforeach
</ul>
</div>
@endif

<form action="{{ route('movies.update',$movies->id) }}" method="POST">
@csrf

 @method('PUT')

	<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12">
	<div class="form-group">
	<strong>Account Name:</strong>
	<input type="text" name="name" value="{{ $movies->name }}"
	class="form-control" placeholder="Account">

</div>
</div>

	<div class="col-xs-12 col-sm-12 col-md-12">
	<div class="form-group">
	<strong>Level:</strong>

	<input type="text" name="review" value="{{ $movies->review }}"
class="form-control" placeholder="Level">


</div>
</div>

<div class="col=xs=12 col-sm-12 col-md-12">
	
	<div class="form-group">
	<strong>Email</strong>
	<input type="text" name="rating" value="{{ $movies->rating }}" class="form-control" placeholder="Email">
</div>
</div>

	<div class="container">
		<div class="row justify-content-between">
			<div class="col-md-2">
			<a class="btn btn-primary" href="{{ route('movies.index') }}">Back</a>
			</div>
			<div class="col-md-2">
			<button type="submit" class="btn btn-primary">Submit</button>
			</div>
			
		</div>
	</div>
</div>

</form>

@endsection