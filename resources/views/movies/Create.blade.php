<nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-4">
  <div class="container-fluid">
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0 mx-auto">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="#">The Martian Movie Review</a>
        </li>
      </ul>
    </div>
  </div>
</nav>
<!--Navbar-->
@extends('movies.layout')
@section('content')
<div class="container-fluid">
    <h1>Write A Review</h1>
    @if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

    <form action="{{ route('movies.store') }}" method="POST">
        @csrf
        <div class="d-flex flex-column">
            <div class="p-2">
                <div class="form-group">
                    <strong>Name:</strong>
                    <input type="text" name="name" class="form-control" placeholder="Name">
                </div>
            </div>
            <div class="p-2">
                <div class="form-group">
                    <strong>Review</strong>
                    <textarea class="form-control" name="review" id="exampleFormControlTextarea1"
                        placeholder="Write Your Review" rows="3"></textarea>
                </div>
            </div>
            <div class="p-2">
                <div class="form-group">
                    <strong>Rating</strong>
                    <input type="text" name="rating" class="form-control" placeholder="Rating">
                </div>
            </div>
        </div>
		<div class="btn-groups">
    <div class="row justify-content-around">
        <div class="col-md-2">
		<a class="btn btn1 btn-primary" href="{{ route('movies.index') }}">Back</a>
        </div>
        <div class="col-md-2">
			<button type="submit" class="btn btn1 btn-primary">Submit</button>
        </div>
    </div>
</div><!--btn-groups-->
    </form>
    @endsection
</div>
<!--Container-->
