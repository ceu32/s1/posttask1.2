<nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-4">
  <div class="container-fluid">
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0 mx-auto">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="#">The Martian Movie Review</a>
        </li>
      </ul>
    </div>
  </div>
</nav>
<!--Navbar End-->
@extends('movies.layout')
@section('content')
<div class="container">
<!--Movie Card-->
    <div class="card mb-4 border border-dark" style="height: auto; width: 100%;">
         <div class="img-con">
         <img src="/img/MartianCovert.jpg" class="img-fluid img card-img-top" alt="The Martian Poster">
         </div>
        <div class="card-body ">
            <h5 class="card-title">The Martian (2015)</h5>
            <p class="card-text">During a manned mission to Mars, Astronaut Mark Watney is presumed dead after a fierce storm and left behind by his crew. But Watney has survived and finds himself stranded and alone on the hostile planet. With only meager supplies, he must draw upon his ingenuity, wit and spirit to subsist and find a way to signal to Earth that he is alive. Millions of miles away, NASA and a team of international scientists work tirelessly to bring "the Martian" home, while his crewmates concurrently plot a daring, if not impossible, rescue mission. As these stories of incredible bravery unfold, the world comes together to root for Watney's safe return.</p>
            <a class="btn btn-success" href="{{ route('movies.create') }}">Write a Review</a>
        </div>
    </div>
@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif
<!--Movie Card End-->

<!--Reviews Page-->
@foreach ($movies as $Movie)
    <div class="card text-center">
        <div class="card-body border border-dark">
            <h5 class="card-title">{{ $Movie->name }}</h5>
            <p class="card-text">{{ $Movie->review }}.</p>
            <p class="">Rating: {{ $Movie->rating }}</p>
            <form action="{{ route('movies.destroy',$Movie->id) }}" method="POST">
                <a class="btn btn-primary" href="{{ route('movies.edit',$Movie->id) }}">Edit</a>
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger">Delete</button>
</div>
    </div>
</div><!--Container Fluid-->
@endforeach
</div>
<!--Reviews Page End-->