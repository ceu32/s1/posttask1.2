<?php

namespace App\Http\Controllers;

use App\Models\Movie;
use Illuminate\Http\Request;

class MovieController extends Controller
{
    public function index()
    {
        $movies=Movie::latest()->paginate(5);
        return view('movies.index',compact('movies'))
        ->with('i',(request()->input('page',1)-1)*5);
    
    }
    
    public function create()
    {
        return view('movies.create');
    }
    
    
    public function store(Request $request)
    {
        $request->validate([
        'name'=>'required',
        'review'=>'required',
        'rating'=>'required',
    
    ]);
    Movie::create($request->all());
    
        return redirect()->route('movies.index')
        ->with('success', 'Students created successfully.');
    }
    
    
    
    public function edit(Movie $movies)
    {
    
        return view('movies.edit',compact('movies'));
    }
    
        public function update(Request $request,Movie $movies)
    {
        $request->validate([
    
    ]);
    
        $movies->update($request->all());
    
        return redirect()->route('movies.index')
        ->with('success','Review Successfully Updated');
    
    }
    
        public function destroy(Movie $movies)
    {
        $movies->delete();
    
        return redirect()->route('movies.index')
        ->with('success','Review Deleted');
    
    }
}
